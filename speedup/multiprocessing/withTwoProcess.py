import time
from multiprocessing import Process

def plusFive(v):
    for i in range(len(v)):
        v[i]= v[i]+5
    return v

def plusFive2(v, start, end):
    for i in range(start,end):
        v[i] = v[i]+5
    return v

vect = [0]*100000000

p1 = Process(target=plusFive2, args=(vect,0,50000000))
p2 = Process(target=plusFive2, args=(vect,50000000,50000000))

startTime = time.time()

p1.start()
p2.start()

p1.join()
p2.join()

endTime = time.time()

print("Finished! Time taken", endTime-startTime)
