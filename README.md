# Programmation paralèlle
Clonez les exercices présents dans https://gitlab.com/fabrice_Hategekimana/parallelisation .

## 1 Speed up
Les exercice de cette section se passent dans le dossier **speedup**

### 1.2 Multithreading (python)
L'exercice se trouve dans le dossier **speedup/python/multithreading**.

1. Inspirez vous des fichiers withTwoThreads.py withFiveThreads.py.
Essayez de résoudre la même tâche avec 10 threads (9 threads appelés + le principal).

2. Comparez le temps obtenu dans ce nouveau fichier avec les autres (single.py, withTwoThreads.py, withFiveThreads.py). Pourquoi le programme ne s'exécute pas proportionnellement plus vite dans ce cas de figure?

### 1.3 Multiprocessing (python)
L'exercice se trouve dans le dossier **speedup/python/processing**.

Faite les mêmes points (point 1 et point 2) que pour l'exercice avec le multithreading en python.

Question supplémentaire: comparez les résultats obtenus avec le multithreading avec les résultats obtenues avec le multiprocessing. Sont-ils différent? Si oui, expliquez d'où viendrait cette différence.

# 2 Multitasking
Nous allons écrire un cli (command line interface) qui aura la capacité de lancer certaines commandes. Le but est de pouvoir donner à l'utilisateur la possibilité de pouvoir envoyer des requêtes en "tâche de fond". Il faudra utiliser le module `multiprocessing` de python pour cet exercice.

Le programme aura ces commandes:

### 2.1 ls
Doit afficher le contenu du dossier dans lequel on se trouve. Il est recommandé d'utiliser listdir.
Vous trouverez des exemples d'implémentation sur: https://www.tutorialspoint.com/python/os_listdir.htm

### 2.2 calc [expression]
Doit pouvoir faire des calcules d'expression comme 2+2= 4. Il vaut mieux se simplifier la tâche et utiliser la fonction eval() de python.
Vous trouverez des exemples d'implémentation sur: https://www.w3schools.com/python/ref_func_eval.asp

### 2.3 play [fichier]
Doit pouvoir lancer le fichier (.mp3 ou .wav). Cette commande doit obligatoirement utiliser le module playsound avec la fonction playsound. 
Vous trouverez des exemples d'implémentation sur: https://pythonbasics.org/python-play-sound/

un processus en arrière plan (utiliser le module multiprocessing de python)
Vous trouverez des exemples d'implémentation sur: https://zetcode.com/python/multiprocessing/

### 2.4 stop 
Arrête la musique en cours s'il elle existe. Doit utiliser la fonction multiprocessing.Process.terminate() 

### 2.5 timer [nombre]
Dois pouvoir faire un timer qui compte un [nombre] de secondes et prévient l'utilisateur lorsqu'il termine. Doit être exécuté en tâche de fond.

